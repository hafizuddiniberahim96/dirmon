from setuptools import setup, find_packages

setup(
    name="dirmon",
    version="3.0.0",
    author="Hafizuddin Iberahim",
    license="Apache License 2.0",
    description="monitoring source files",
    packages=find_packages(),
    package_data={'dirmon': ['dirmon.stoq']},
    include_package_data=True,
)
